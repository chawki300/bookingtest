import * as React from 'react'
import { Provider } from 'react-redux'
import configureStore from './redux/store'

import Home from './containers/home'

const store = configureStore()

const Root = () => (
  <Provider store={store}>
    <Home />
  </Provider>
)
export default Root
