import React from 'react'
import { connect } from 'react-redux'
import { onHandlePannier } from '../redux/pannier/actions'
import { AppState } from '../redux/store'
import { HotelState } from './../redux/pannier/types'

export interface CardProps {
  id: number
  title: string
  paragraph: string
  price: number
  priceInfo: string
  image: string
  onHandlePannier: typeof onHandlePannier
  hotel: HotelState
  pannierStyle: boolean
  key: number
}

const Card = ({
  title,
  paragraph,
  image,
  price,
  priceInfo,
  id,
  onHandlePannier,
  pannierStyle,
  key,
}: CardProps) => {
  return (
    <div
      onClick={() => {
        onHandlePannier({
          id,
          counter: pannierStyle ? -1 : 1,
          numberOfNights: 1,
          pricePerNight: price,
        })
      }}
      className={pannierStyle ? 'pcard' : 'card'}
      key={key}
    >
      <div>
        <img src={image} alt={'hotel img'} className={pannierStyle ? 'pimageCard' : 'imageCard'} />
      </div>
      <div className="card-container">
        <div className="info-container">
          <div>{!pannierStyle && <h4>{title}</h4>}</div>
          <div>
            <h4>{price}£</h4>
            <p className="price-style">{priceInfo}</p>
          </div>
        </div>

        {!pannierStyle && <p className="card-paragraph">{paragraph}</p>}
      </div>
    </div>
  )
}
Card.defaultProps = {
  title: 'Hotel de la plage',
  paragraph:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit.sus ac,  amet sollicitudin condimentum. Sed et rutrum quam, et euismod augue. Proin fermentum massa sed dolor porta sodales. Etiam sit amet tellus efficitur, pulvinar elit vel, lacinia ex. Ut sit amet turpis dapibus, molestie turpis et, dictum magna. Suspendisse scelerisque erat vel mollis iaculis. Morbi non euismod magna, sed pharetra tellus.    ',
  image:
    'https://www.yonder.fr/sites/default/files/styles/scale_1008x672/public/destinations/A-Burj-Al-Arab-Dubai-1.jpg?itok=5Ce1ksCP',
  price: 400,
  priceInfo: 'nuit/jour',
  pannierStyle: false,
} as Partial<CardProps>

const mapStateToProps = (state: AppState) => ({
  hotels: state.pannier,
})

export default connect(
  mapStateToProps,
  { onHandlePannier }
)(Card)
