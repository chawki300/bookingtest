import React, { Fragment, useRef } from 'react'
import { connect } from 'react-redux'
import data from '../assets/data.json'
import Card from '../components/card'
import { onHandleModal } from '../redux/modal/actions'
import { onHandlePannier } from '../redux/pannier/actions'
import { PannierState } from '../redux/pannier/types'
import { AppState } from '../redux/store'
/*eslint no-lone-blocks: 0*/

export interface PannierProps {
  onHandleModal: typeof onHandleModal
  pannier: PannierState
}

const Pannier = ({ onHandleModal, pannier }: PannierProps) => {
  let cards: any[] | JSX.Element[] = []
  const myRef = useRef<HTMLInputElement>(null)
  cards = []
  pannier.hotels.forEach(hotel => {
    {
      for (let i = 0; i < hotel.numberOfNights; i++) {
        cards.push(<Card {...data[hotel.id]} pannierStyle={true} />)
      }
    }
  })
  if (myRef.current) {
    myRef.current.scrollIntoView({
      behavior: 'smooth',
    })
  }
  return (
    <div
      style={{
        width: '100%',
        height: '100vh',
        backgroundColor: '#EBEBEB',
        padding: '10px',
        overflowY: 'auto',
      }}
      id={'pannier'}
    >
      <h3 style={{ color: '#292929' }}>Pannier</h3>
      {cards}

      {pannier.total! > 0 && (
        <Fragment>
          <div
            ref={myRef}
            style={{
              backgroundColor: '#0FDB00',
              width: '60%',
              height: '40px',
              borderRadius: '5px',
              float: 'right',
            }}
          >
            <p
              style={{
                padding: 'auto',
                color: '#176011',
                fontSize: '12px',
                fontWeight: 'bold',
              }}
            >
              TOTAL <span style={{ fontSize: '20px' }}>{pannier.total}£</span>{' '}
            </p>
          </div>
          <button className="buttonC" onClick={() => onHandleModal({ open: true, children: null })}>
            <span>Confirmer </span>
          </button>
        </Fragment>
      )}
    </div>
  )
}
Pannier.defaultProps = {
  title: 'Booking',
} as Partial<PannierProps>

const mapStateToProps = (state: AppState) => ({
  pannier: state.pannier,
})

export default connect(
  mapStateToProps,
  { onHandleModal, onHandlePannier }
)(Pannier)
