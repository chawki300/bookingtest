import React from 'react'
import { onHandleAlert } from '../redux/alert/actions'
import { onHandleModal } from '../redux/modal/actions'

export interface FormProps {
  onHandleModal?: typeof onHandleModal
  onHandleAlert?: typeof onHandleAlert
}

const Form = ({ onHandleModal, onHandleAlert }: FormProps) => {
  return (
    <div className="container">
      <form
        onSubmit={() => {
          onHandleModal!({ open: false, children: null })
          onHandleAlert!({ open: true })
        }}
      >
        <div className="row">
          <div className="col-25">
            <label htmlFor="fname">Nom</label>
          </div>
          <div className="col-75">
            <input type="text" id="fname" name="firstname" placeholder="Votre nom ..." />
          </div>
        </div>
        <div className="row">
          <div className="col-25">
            <label htmlFor="lname">Prenom</label>
          </div>
          <div className="col-75">
            <input type="text" id="lname" name="lastname" placeholder="Votre prenom ..." />
          </div>
        </div>
        <div className="row">
          <div className="col-25">
            <label htmlFor="lname">Email</label>
          </div>
          <div className="col-75">
            <input
              type="email"
              id="lname"
              name="lastname"
              placeholder="Votre adresse email..."
              required={true}
            />
          </div>
        </div>

        <div className="row ">
          <input className={'submit'} type="submit" value="Passer la commande" />
        </div>
      </form>
    </div>
  )
}

export default Form
