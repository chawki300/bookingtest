import React from 'react'
import ProfilePicture from '../assets/img/profile.jpg'
export interface ProfileProps {
  picture: any
  name: string
}

const Profile = ({ picture, name }: ProfileProps) => {
  return (
    <div
      style={{
        width: '100%',
        height: '30px',
        backgroundColor: '#FFFFFF',
        flexDirection: 'row',
        display: 'flex',
        minWidth: '130px',
        borderRadius: '5px',
        paddingTop: '5px',
        marginTop: '10px',
      }}
    >
      <div style={{ alignItems: 'center', justifyContent: 'start', flex: 1 }}>
        <img
          src={picture}
          style={{
            marginLeft: '4px',
            height: '30px',
            width: '30px',
            borderRadius: '15px',
          }}
          alt="logo"
        />
      </div>
      <div style={{ alignItems: 'center', justifyContent: 'center', flex: 2 }}>
        <h5 style={{ textAlign: 'center', paddingTop: '5px' }}>{name}</h5>
      </div>
    </div>
  )
}
Profile.defaultProps = {
  picture: ProfilePicture,
  name: 'Jhon Doe',
} as Partial<ProfileProps>

export default Profile
