import Button from '@material-ui/core/Button'
import Snackbar, { SnackbarOrigin } from '@material-ui/core/Snackbar'
import React from 'react'
import { onHandleAlert } from './../redux/alert/actions'
import { AlertState } from './../redux/alert/types'

export interface State extends SnackbarOrigin {
  open: boolean
}
interface AlertProps {
  alert: AlertState
  onHandleAlert: typeof onHandleAlert
}

const Alert = ({ alert, onHandleAlert }: AlertProps) => {
  const [state] = React.useState<State>({
    open: true,
    vertical: 'top',
    horizontal: 'right',
  })
  const { vertical, horizontal, open } = state

  const handleClose = () => {
    onHandleAlert({ open: false })
  }

  return (
    <div>
      <Snackbar
        anchorOrigin={{ vertical, horizontal }}
        key={`${vertical},${horizontal}`}
        open={open && alert.open}
        onClose={handleClose}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={<span id="message-id">Traitement terminé avec succès</span>}
      />
    </div>
  )
}
export default Alert
