import React from 'react'
import logoReact from '../assets/img/logo.svg'
import Profile from './profile'
export interface HeaderProps {
  logo: any
}

const Header = ({ logo }: HeaderProps) => {
  return (
    <div
      style={{
        width: '100%',
        height: '60px',
        backgroundColor: '#929292',
        flexDirection: 'row',
        display: 'flex',
      }}
    >
      <div style={{ left: '8%', position: 'absolute' }}>
        <img src={logo} style={{ height: '60px', width: '100px' }} alt="logo" />
      </div>
      <div style={{ right: '8%', position: 'absolute' }}>
        <Profile />
      </div>
    </div>
  )
}
Header.defaultProps = {
  logo: logoReact,
} as Partial<HeaderProps>

export default Header
