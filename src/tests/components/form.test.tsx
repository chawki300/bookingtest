import { shallow } from 'enzyme'
import React from 'react'
import Form from '../../components/form'
describe('MyComponent', () => {
  it('should render correctly in "debug" mode', () => {
    const wrapper = shallow(<Form />)

    expect(wrapper).toMatchSnapshot()
  })
})
