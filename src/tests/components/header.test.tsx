import { shallow } from 'enzyme'
import React from 'react'
import Header from '../../components/header'
describe('MyComponent', () => {
  it('should render correctly in "debug" mode', () => {
    const component = shallow(<Header />)
    expect(component).toMatchSnapshot()
  })
})
