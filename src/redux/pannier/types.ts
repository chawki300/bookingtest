export interface HotelState {
  id: number
  numberOfNights: number
  counter: number
  pricePerNight?: number
}
export interface PannierState {
  hotels: HotelState[]
  total?: number
}
export const ON_HANDLE_PANNIER = 'ON_HANDLE_PANNIER'

interface OnHandlePannierAction {
  type: typeof ON_HANDLE_PANNIER
  payload: HotelState
}

export type PannierActionTypes = OnHandlePannierAction
