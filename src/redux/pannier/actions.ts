import { HotelState, ON_HANDLE_PANNIER } from './types'

export function onHandlePannier(newHotelState: HotelState) {
  return {
    type: ON_HANDLE_PANNIER,
    payload: newHotelState,
  }
}
