import { HotelState, ON_HANDLE_PANNIER, PannierActionTypes, PannierState } from './types'

const initialState: PannierState = {
  hotels: [],
}

function calculateTotal(element: PannierState): number {
  let sum = 0

  element.hotels.forEach((h: HotelState) => {
    sum = h.numberOfNights * h.pricePerNight! + sum
  })
  return sum
}

export function pannierReducer(state = initialState, action: PannierActionTypes): PannierState {
  switch (action.type) {
    case ON_HANDLE_PANNIER:
      if (action.payload.counter > 0) {
        if (state.hotels.filter(e => e.id === action.payload.id).length > 0) {
          const h1 = state.hotels.map((h: HotelState) => {
            if (h.id === action.payload.id) {
              return {
                id: h.id,
                numberOfNights: h.numberOfNights + 1,
                counter: 1,
                pricePerNight: h.pricePerNight,
              }
            } else {
              return h
            }
          })
          return {
            hotels: h1,
            total: calculateTotal({ hotels: h1 }),
          }
        } else {
          return {
            hotels: [...state.hotels.concat(action.payload)],
            total: calculateTotal({
              hotels: [...state.hotels.concat(action.payload)],
            }),
          }
        }
      } else {
        if (action.payload.counter < 0) {
          if (state.hotels.filter(e => e.id === action.payload.id).length > 0) {
            const h1 = state.hotels.map((h: HotelState) => {
              if (h.id === action.payload.id) {
                return {
                  id: h.id,
                  numberOfNights: h.numberOfNights - 1,
                  counter: 1,
                  pricePerNight: h.pricePerNight,
                }
              } else {
                return h
              }
            })
            return {
              hotels: h1,
              total: calculateTotal({ hotels: h1 }),
            }
          } else {
            return {
              hotels: [...state.hotels.concat(action.payload)],
              total: calculateTotal({
                hotels: [...state.hotels.concat(action.payload)],
              }),
            }
          }
        }
      }
    /* falls through */
    default:
      return state
  }
}
