import { AlertState, ON_HANDLE_ALERT } from './types'

export function onHandleAlert(newAlertState: AlertState) {
  return {
    type: ON_HANDLE_ALERT,
    payload: newAlertState,
  }
}
