import { AlertActionTypes, AlertState, ON_HANDLE_ALERT } from './types'

const initialState: AlertState = {
  open: false,
}

export function alertReducer(state = initialState, action: AlertActionTypes): AlertState {
  console.log(action)
  switch (action.type) {
    case ON_HANDLE_ALERT:
      return {
        open: action.payload.open,
      }

    default:
      return state
  }
}
