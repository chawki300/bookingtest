export interface AlertState {
  open: boolean
}

export const ON_HANDLE_ALERT = 'ON_HANDLE_ALERT'

interface OnHandleAlertAction {
  type: typeof ON_HANDLE_ALERT
  payload: AlertState
}

export type AlertActionTypes = OnHandleAlertAction
