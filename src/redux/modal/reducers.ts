import { ModalActionTypes, ModalState, ON_HANDLE_MODAL } from './types'

const initialState: ModalState = {
  open: false,
  children: null,
}

export function modalReducer(state = initialState, action: ModalActionTypes): ModalState {
  console.log(action)
  switch (action.type) {
    case ON_HANDLE_MODAL:
      return {
        open: action.payload.open,
        children: action.payload.children,
      }

    default:
      return state
  }
}
