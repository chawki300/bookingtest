import { ModalState, ON_HANDLE_MODAL } from './types'

export function onHandleModal(newModalState: ModalState) {
  return {
    type: ON_HANDLE_MODAL,
    payload: newModalState,
  }
}
