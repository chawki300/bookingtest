export interface ModalState {
  open: boolean
  children: any
}

export const ON_HANDLE_MODAL = 'ON_HANDLE_MODAL'

interface OnHandleModalAction {
  type: typeof ON_HANDLE_MODAL
  payload: ModalState
}

export type ModalActionTypes = OnHandleModalAction
