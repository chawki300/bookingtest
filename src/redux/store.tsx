import { applyMiddleware, combineReducers, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { createLogger } from 'redux-logger'
import { alertReducer } from './alert/reducers'
import { modalReducer } from './modal/reducers'

import { pannierReducer } from './pannier/reducers'

const logger = createLogger({
  collapsed: true,
  duration: true,
  diff: true,
})

const rootReducer = combineReducers({
  modal: modalReducer,
  pannier: pannierReducer,
  alert: alertReducer,
})

export type AppState = ReturnType<typeof rootReducer>

export default function configureStore() {
  const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(logger)))

  return store
}
