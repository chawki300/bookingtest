import { Modal } from '@material-ui/core'
import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import '../App.scss'
import data from '../assets/data.json'
import Alert from '../components/alert'
import Card from '../components/card'
import Form from '../components/form'
import Header from '../components/header'
import Pannier from '../components/pannier'
import { onHandleAlert } from '../redux/alert/actions'
import { onHandleModal } from '../redux/modal/actions'
import { AppState } from '../redux/store'
import { AlertState } from './../redux/alert/types'
import { ModalState } from './../redux/modal/types'

interface AppProps {
  onHandleModal: typeof onHandleModal
  onHandleAlert: typeof onHandleAlert
  modal: ModalState
  alert: AlertState
}

const Home: React.SFC<AppProps> = ({ modal, onHandleModal, alert, onHandleAlert }) => {
  return (
    <div className="App noselect">
      <Header />
      <div className="App-container">
        <div style={{ flex: 4 }}>
          {data.map((hotel, i) => (
            <Card {...hotel} key={i} />
          ))}
        </div>
        <div style={{ flex: 1 }}>
          <Pannier />
        </div>
      </div>
      <Modal
        open={modal.open}
        onBackdropClick={() => onHandleModal({ open: false, children: null })}
      >
        <Fragment>
          <Form onHandleModal={onHandleModal} onHandleAlert={onHandleAlert} />
        </Fragment>
      </Modal>
      <Alert alert={alert} onHandleAlert={onHandleAlert} />
    </div>
  )
}

const mapStateToProps = (state: AppState) => ({
  modal: state.modal,
  alert: state.alert,
})

export default connect(
  mapStateToProps,
  { onHandleModal, onHandleAlert }
)(Home)
